#!/bin/bash

sudo yum -y update
sudo yum -y install nginx
sudo aws s3 cp s3://cogni-nonprod-assignment-himathh-s3/himath.local.conf /etc/nginx/conf.d/
sudo systemctl restart nginx
sudo mkdir /var/www/himath.local/
sudo aws s3 cp s3://cogni-nonprod-assignment-himathh-s3/index.html /var/www/himath.local/
sudo chmod 755 -R /var/www/himath.local/index.html
