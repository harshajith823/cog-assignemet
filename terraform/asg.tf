resource "aws_key_pair" "auth" {
  key_name   = "hima_key"
  public_key = "${file("/home/ec2-user/.ssh/hima_key.pub")}"
}

resource "aws_launch_configuration" "autoscale_launch" {
  image_id = "ami-01f765573b7626c4b"
  instance_type = "t2.micro"
  iam_instance_profile = "arn:aws:iam::317323733298:instance-profile/cogni-nonprod-himathh-iam"
  security_groups = ["${aws_security_group.sec_web.id}"]
  key_name = "${aws_key_pair.auth.id}"
#  associate_public_ip_address = true
  user_data = "${file("user_data.sh")}"
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "autoscale_group" {
  launch_configuration = "${aws_launch_configuration.autoscale_launch.id}"
  vpc_zone_identifier = ["subnet-0bfe2ee87f5b4c782","subnet-0a805a2f6013ed716","subnet-07ef8f621e5436c58"]
  load_balancers = ["${aws_elb.elb.name}"]
  min_size = 3
  max_size = 3
  tag {
    key = "Name"
    value = "autoscale"
    propagate_at_launch = true
  }
}
